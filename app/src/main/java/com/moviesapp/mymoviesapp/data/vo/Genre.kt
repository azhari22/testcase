package com.moviesapp.mymoviesapp.data.vo

data class Genre(
    val id : Int,
    val name : String
)